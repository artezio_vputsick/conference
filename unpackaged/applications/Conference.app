<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Interview_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
        <pageOrSobjectType>Interview__c</pageOrSobjectType>
    </actionOverrides>
    <brand>
        <headerColor>#0070D2</headerColor>
        <shouldOverrideOrgTheme>false</shouldOverrideOrgTheme>
    </brand>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Conference</label>
    <navType>Standard</navType>
    <tabs>Interviewer__c</tabs>
    <tabs>Interview_Interviewer__c</tabs>
    <tabs>Venue__c</tabs>
    <tabs>Session__c</tabs>
    <tabs>Audience__c</tabs>
    <tabs>Conference__c</tabs>
    <tabs>Interview__c</tabs>
    <tabs>Speaker_Session__c</tabs>
    <tabs>standard-report</tabs>
    <tabs>standard-Dashboard</tabs>
    <tabs>Hotel__c</tabs>
    <tabs>Speaker__c</tabs>
    <tabs>Review__c</tabs>
    <tabs>Booking__c</tabs>
    <tabs>Feedback__c</tabs>
    <uiType>Lightning</uiType>
    <utilityBar>Conference_UtilityBar</utilityBar>
</CustomApplication>
