<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Speaker_Approval_Alert</fullName>
        <description>Speaker Approval Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Approve_Conference</template>
    </alerts>
    <alerts>
        <fullName>Speaker_Interview_Approve</fullName>
        <description>Speaker Interview Approve</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Notify_Speaker_About_Interview_Result</template>
    </alerts>
</Workflow>
