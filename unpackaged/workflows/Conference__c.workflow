<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Approve_Expense</fullName>
        <field>Stage__c</field>
        <literalValue>Wait Audience</literalValue>
        <name>Approve Expense</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approve_Expenses</fullName>
        <field>Stage__c</field>
        <literalValue>Wait Audience</literalValue>
        <name>Approve Expenses</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved</fullName>
        <field>Stage__c</field>
        <literalValue>Wait Audience</literalValue>
        <name>Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Revert_Stage</fullName>
        <field>Stage__c</field>
        <literalValue>Planning Confrerece</literalValue>
        <name>Revert Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Revert_To_Planing</fullName>
        <field>Stage__c</field>
        <literalValue>Planning Confrerece</literalValue>
        <name>Revert To Planing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
